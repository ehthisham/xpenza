angular.module('app.services', [])

.factory('LocalStorage', [function(){
    var ls={
        store:store,
        get:get
    };
    return ls;
    function store(collection,data){
        if(typeof data === 'object'){
            window.localStorage[collection] = JSON.stringify(data);
        }
        else{
            window.localStorage[collection]=data;
        }
    }
    function get(collection){
        var data =window.localStorage[collection] || "";
        if(data.length){
             if(JSON.parse(data)){
            return JSON.parse(data);
        }
        else return data;
        }
        else return [];
    }
}])
.factory('global',[function(){
    return {
        content:1,
        expence:[],
        income:[],
        expence_total:0,
        income_total:0,
    };
}])
.service('BlankService', [function(){

}]);

