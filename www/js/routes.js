angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    
      
    .state('menu', {
      cache:false,
      url: '',
      abstract:true,
      templateUrl: 'templates/menu.html'
    })
      

    .state('menu.addExpence', {
      cache:false,
      url: '/addexpence',
      views: {
        'side-menu21': {
          templateUrl: 'templates/addExpence.html',
          controller: 'addExpenceCtrl'
        }
      }
    })
        
        
    .state('menu.addIncome', {
      cache:false,
      url: '/addincome',
      views: {
        'side-menu21': {
          templateUrl: 'templates/addIncome.html',
          controller: 'addIncomeCtrl'
        }
      }
    })
        
 
    .state('menu.graph', {
      cache:false,
      url: '/graph',
      views: {
        'side-menu21': {
          templateUrl: 'templates/graph.html',
          controller: 'graphCtrl'
        }
      }
    })
        
    .state('menu.home', {
      cache:false,
      url: '/xspenza',
      views: {
        'side-menu21': {
          templateUrl: 'templates/home.html',
          controller: 'homeCtrl'
        }
      }
    })

    .state('menu.about', {
      cache:false,
      url: '/about',
      views: {
        'side-menu21': {
          templateUrl: 'templates/about.html',
          controller: 'aboutCtrl'
        }
      }
    })
        
      
    ;

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/xspenza');

});
