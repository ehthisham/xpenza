angular.module('app.controllers', [])
     
.controller('addExpenceCtrl', function($scope,LocalStorage,global,$state) {
    $scope.expense={
        amount:0,
        title:"",
        category:"",
        date:""
    };
    $scope.set=function(cat){
        console.log(cat);
        $scope.expense.category=cat;
    }
    $scope.add=function(data){
//        global.expence_total=parseInt(global.expence_total)+parseInt(data.amount);
        console.log(global.expence_total,data.amount);
        var results=LocalStorage.get('expense_data');
        results.push(data);
        global.expence.push(data);
        LocalStorage.store('expense_data',results);
        global.content=1;
        $scope.expense={
        expence:0,
        title:"",
        category:"",
        date:""
    };
        $state.go('menu.home',{cache: false});
    }

})

.controller('aboutCtrl', function($scope,LocalStorage,global,$state) {

})
   
.controller('addIncomeCtrl', function($scope,LocalStorage,global,$state) {
    $scope.income={
        amount:0,
        title:"",
        category:"",
        date:""
    };
    $scope.set=function(cat){
        $scope.income.category=cat;
    }
    $scope.add=function(data){
//        global.income_total+=parseInt(data.amount);
        var results=LocalStorage.get('income_data');
        results.push(data);
        global.income.push(data);
        LocalStorage.store('income_data',results);
        global.content=2;
        $scope.income={
        income:0,
        title:"",
        category:"",
        date:""
    };
        $state.go('menu.home',{cache: false})
    }
})
   
.controller('graphCtrl', function($scope,global,LocalStorage) {
    $scope.content=global.content;
    
    $scope.expense_data=global.expence = LocalStorage.get('expense_data');
    $scope.income_data=global.income = LocalStorage.get('income_data');
    var exp_result={};
    console.log(global.expence);
    var colors=['#F7464A','#46BFBD','#FDB45C','#7c7c7c','#6795f3','#fc67de','#85e39d'];
    global.expence.forEach(function(el,i){
        if(typeof exp_result[el.category]=='undefined'){
            exp_result[el.category]={value:parseInt(el.amount),label:el.category,color:colors[i],highlight:colors[i]};
        }
        else{
            exp_result[el.category].value+=parseInt(el.amount);
        }
    });
    var i=0;
   $scope.result1= _.map(exp_result,function(o,i,j){return o});
    console.log($scope.result1);
    var inc_result={};
    global.income.forEach(function(el,i){
        if(typeof exp_result[el.category]=='undefined'){
            inc_result[el.category]={value:parseInt(el.amount),label:el.category,color:colors[i],highlight:colors[i]};
        }
        else{
            inc_result[el.category].value+=parseInt(el.amount);
        }
    });
    var j=0;
    $scope.result2= _.map(inc_result,function(o){return o});
    console.log($scope.result1);
    // Chart.js Options
    $scope.options =  {

      // Sets the chart to be responsive
      responsive: false,

      //Boolean - Whether we should show a stroke on each segment
      segmentShowStroke : false,

      //Number - The percentage of the chart that we cut out of the middle
      percentageInnerCutout : 50, // This is 0 for Pie charts

      //Number - Amount of animation steps
      animationSteps : 100,

      //String - Animation easing effect
      animationEasing : 'easeOutBounce',

      //Boolean - Whether we animate the rotation of the Doughnut
      animateRotate : true,

      //Boolean - Whether we animate scaling the Doughnut from the centre
      animateScale : false,

      //String - A legend template
      legendTemplate : '<ul class="tc-chart-js-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'

    };

})
   
.controller('homeCtrl', function($scope,global,LocalStorage,$rootScope,$state) {
    $scope.display_month=false;
    console.log('homeCtrl');
    $scope.expence=global.expence_total;
    $scope.income=global.income_total;
    $scope.content=global.content;
    global.expence=LocalStorage.get('expense_data');
    $scope.expense_data=global.expence;
    global.income=LocalStorage.get('income_data');
    $scope.income_data=global.income;
    $scope.expense_data.forEach(function(el){
        $scope.expence+=parseInt(el.amount);
    });
    $scope.income_data.forEach(function(el){
        $scope.income+=parseInt(el.amount);
    });
    $scope.more=0;
    $scope.month="";
    $scope.select=function(month){
        $scope.month=month;
        $scope.display_month=false;
//        console.log($scope.month,$scope.more);
    }
    $scope.go_to_graph=function(){
        global.content=$scope.content;
        console.log($scope.content);
        $state.go('menu.graph');
    }
})
 