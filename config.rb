require "susy"
require 'breakpoint'


http_path = "/"
# Require any additional compass plugins here.

#Folder settings
relative_assets = true      #because we're not working from the root
css_dir = "/www/lib/ionic/css"          #where the CSS will saved
sass_dir = "scss"           #where our .scss files are
images_dir = "../images"    #the folder with your images
fonts_dir = "fonts"


# You can select your preferred output style here (can be overridden via the command line):
output_style = :expanded # After dev :compressed

# To disable debugging comments that display the original location of your selectors. Uncomment:
line_comments = false

# Obviously
preferred_syntax = :scss
